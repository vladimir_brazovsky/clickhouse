#!/bin/bash
set -e

if [ -e /val/log/clickhouse-server/nofirstrun ]; 
then echo ""
else
clickhouse client -n <<-EOSQL
	CREATE DATABASE IF NOT EXISTS analytics;
	CREATE TABLE IF NOT EXISTS analytics.events
   			(
        			id String,
        			event_time DateTime,
        			event_message String
    			)
    			ENGINE = MergeTree
    			PARTITION BY toYYYYMM(event_time)
    			ORDER BY (event_time)
    			SETTINGS index_granularity = 8192;
   	INSERT INTO analytics.events SELECT * FROM file('events.csv','CSV','column1 String, column2 DateTime, column3 String');
    	
EOSQL

touch /val/log/clickhouse-server/nofirstrun
fi
